import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.scss']
})
export class Page1Component implements OnInit {

  //Interpolation :

  myName: string = 'Chtrist';
  myAge: number = 20;
  isBeauGoss:boolean = true  ;


  // property binding


  person:string="Marco Polo";
  age:number = 45;
  address:any ={
    street:"rue du Paradis",
    city:"75000 Paris"
  };

  alignement:string = 'right';

  //Event Binding

  mot:string = "urgent";
  alert:string = "alert alert-danger"
  isDanger:boolean = true;
  changeMot(){
    this.isDanger = !this.isDanger;
    this.alert = this.isDanger ? "alert alert-danger" : "alert alert-success";
    this.mot = this.isDanger ? "Urgent" : "Pas urgent";
  }


modification:string = " ";

modifie(valeur:any){


  console.log(valeur.target.value);
  this.modification = valeur.target.value;



}

modificationEncore:number =0;

modifieEncore(valeur:any){
  console.log(valeur);
  this.modificationEncore = valeur.target.value;
}

focused(){
  console.log('focus')
}

//Two Way Binding

userName: string = "Michou";

  constructor() { }

  ngOnInit(): void {
  }

}
