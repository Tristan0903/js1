var planetes;
(function (planetes) {
    planetes[planetes["MERCURE"] = 0] = "MERCURE";
    planetes[planetes["VENUS"] = 1] = "VENUS";
    planetes[planetes["TERRE"] = 2] = "TERRE";
    planetes[planetes["MARS"] = 3] = "MARS";
    planetes[planetes["JUPITER"] = 4] = "JUPITER";
    planetes[planetes["SATURNE"] = 5] = "SATURNE";
    planetes[planetes["URANUS"] = 6] = "URANUS";
    planetes[planetes["NEPTUNE"] = 7] = "NEPTUNE";
})(planetes || (planetes = {}));
var age = 25;
var ageannées;
var agesecondes = age * 31557600;
var userPlanet = "NEPTUNE";
if (userPlanet === planetes[0]) {
    agesecondes = agesecondes * 7600543;
}
else if (userPlanet === planetes[1]) {
    agesecondes = agesecondes * 19414149;
}
else if (userPlanet === planetes[2]) {
    agesecondes = agesecondes * 1;
}
else if (userPlanet === planetes[3]) {
    agesecondes = agesecondes * 59354032;
}
else if (userPlanet === planetes[4]) {
    agesecondes = agesecondes * 374355659;
}
else if (userPlanet === planetes[5]) {
    agesecondes = agesecondes * 929292362;
}
else if (userPlanet === planetes[6]) {
    agesecondes = agesecondes * 2651370019;
}
else if (userPlanet === planetes[7]) {
    agesecondes = agesecondes * 5200418560;
}
ageannées = agesecondes / 31557600;
console.log(ageannées);
