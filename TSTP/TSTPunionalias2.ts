type int = number;
type float = number;
type numberArray = number[];
type stringArray = string[];

var var1:int;
var var2:float;
var var3:numberArray;
var var4:stringArray;

var1 = 12;
var2= 15,9287;
var3=[1,5,4,7,8,7];
var4=["toto", "titi", "tata"];

console.log("La valeur de la variable 1 de type int est de: " +var1);
console.log("La valeur de la variable 2 de type float est de: " +var2);

for (let i=0; i<var3.length; i++){
    console.log(var3[i]);
}

for (let j=0; j<var4.length; j++){
    console.log(var4[j]);
}

