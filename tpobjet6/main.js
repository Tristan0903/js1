

var success = document.querySelector('#success');
var input = document.querySelector('#input');
var bouton = document.querySelector('#bouton');
var produits = document.querySelector('#produits');
var vidlist = document.querySelector('#vidlist');
var danger = document.getElementById("danger");

function inputValidation(){
    if (input.value === ""){

        success.style.display = "none";
        danger.style.display = "block";
        danger.textContent = "You must complete the input";       
        
    } else {
        danger.style.display = "none";
        success.style.display = "block";
        success.textContent = "Item Added To The List";
        ajouterProduit();
    } 
};

function ajouterProduit(){
    localStorage.setItem("produit", (input.value) );
    ajout = localStorage.getItem("produit");
    creerProduit();
    ajoutVideList();
    boutonSubmit();
}

function creerProduit(){
    produits.innerHTML += `
    <div class="d-flex justify-content-between">
      <p>${ajout}</p>
      <div class="options d-flex gap-3">
        <i onClick="editProduit(this)" class="bi bi-pencil-square"></i>
        <i onClick="deleteProduit(this)" class="bi bi-trash3-fill"></i>
      </div>
    </div>
    `;
    input.value = "";
  };


  function ajoutVideList(){
    vidlist.innerHTML = "Vider la liste";
  }

  let deleteProduit = (i) => {
    i.parentElement.parentElement.remove();
};

let editProduit = (i) => {
    input.value = i.parentElement.previousElementSibling.innerHTML;
    i.parentElement.parentElement.remove();
    boutonEdit()
}


function boutonSubmit(){
    bouton.textContent = "Submit";
}

function boutonEdit(){
    bouton.textContent = "Edit";
}

